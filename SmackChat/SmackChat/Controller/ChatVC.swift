//
//  ChatVC.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/9/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import UIKit

class ChatVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    //Outlets
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var channelLbl: UILabel!
    @IBOutlet weak var messageTxtBox: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var typingStatusLbl: UILabel!
    //Variables
    var isTyping: Bool = false
    var curChannel: String?
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        //Add some notification Observer
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.userDataDidChange(_:)), name: NOTIF_USERDATA_DID_CHANGE, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.channelSelected(_:)), name: NOTIF_CHANNEL_SELECTED, object: nil)
        
        //
        SocketService.instance.getChatMessage { (newMesssage) in
            if newMesssage.channelId == MessageService.instance.selectedChannel?._id && AuthService.instance.isLoggedIn {
                MessageService.instance.messages.append(newMesssage)
                self.tableView.reloadData()
                //
                if MessageService.instance.messages.count > 0 {
                    let endIndex = IndexPath(row: MessageService.instance.messages.count - 1, section: 0)
                    self.tableView.scrollToRow(at: endIndex, at: .bottom, animated: true)
                }
            }
        }
//        SocketService.instance.getChatMessage { (success) in
//            if success {
//                self.tableView.reloadData()
//                if MessageService.instance.messages.count > 0 {
//                    let endIndex = IndexPath(row: MessageService.instance.messages.count - 1, section: 0)
//                    self.tableView.scrollToRow(at: endIndex, at: .bottom, animated: true)
//                }
//            }
//        }
        //
        SocketService.instance.getTypingUsers { (typingUsers) in
            guard let channelId = MessageService.instance.selectedChannel?._id else {return}
            var names = ""
            var numberOfTypers = 0
            for (typingUser, channel) in typingUsers {
                if typingUser != UserDataService.instance.name && channel == channelId {
                    if names == "" {
                        names = typingUser
                    }
                    else {
                        names = "\(names), \(typingUser)"
                    }
                    numberOfTypers += 1
                }
            }
            if numberOfTypers > 0 && AuthService.instance.isLoggedIn {
                let verb = numberOfTypers > 1 ? "are" : "is"
                self.typingStatusLbl.text = "\(names) \(verb) typing..."
            }
            else {
                self.typingStatusLbl.text = ""
            }
        }
        
        
        //Check and load user data in case logged in
        if AuthService.instance.isLoggedIn {
            //for logged_In user on the last sesssion
            AuthService.instance.findUserByEmail { (success) in
                if success {
                NotificationCenter.default.post(name: NOTIF_USERDATA_DID_CHANGE, object: nil)
                }
            }
        }
    }
    @objc func userDataDidChange(_ notif: Notification){
        if AuthService.instance.isLoggedIn {
            onLoginGetMessages()
        }
        else {
            self.tableView.reloadData();
             channelLbl.text = "Please Log In"
        }
    }
    @objc func channelSelected (_  notif: Notification){
        updateWithChannel()
    }
    func updateWithChannel(){
        //Update channel Title
        self.channelLbl.text = "#\(MessageService.instance.selectedChannel?.name! ?? "Smack")"
        //Load messages
        getMessages()
    }
    
    @IBAction func messageBoxEditing(_ sender: Any) {
        guard let channelId = MessageService.instance.selectedChannel?._id else {return}
        if messageTxtBox.text == "" {
            self.isTyping = false
            sendBtn.isHidden = true
            SocketService.instance.socketM.defaultSocket.emit("stopType", UserDataService.instance.name, channelId)
        }else{
            if self.isTyping == false {
                sendBtn.isHidden = false
                SocketService.instance.socketM.defaultSocket.emit("startType", UserDataService.instance.name, channelId)
            }
            self.isTyping = true
        }
    }
    @IBAction func sendMessagePressed(_ sender: Any) {
        if AuthService.instance.isLoggedIn {
            guard let channelId = MessageService.instance.selectedChannel?._id else {return}
            guard let message = messageTxtBox.text else {return}
            
            SocketService.instance.sendMessage(messageBody: message, userId: UserDataService.instance.id, channelId: channelId) { (success) in
                if success {
                    self.messageTxtBox.text = ""
                    self.sendBtn.isHidden = true
                    self.isTyping = false
                    
                    SocketService.instance.socketM.defaultSocket.emit("stopType", UserDataService.instance.name, channelId)
                    self.messageTxtBox.resignFirstResponder()
                    //
                }
            }
        }
    }
    func onLoginGetMessages(){
        MessageService.instance.findAllChannels { (success) in
            
            if success {
                if MessageService.instance.channels.count > 0 {
                    //Get message
                    MessageService.instance.selectedChannel = MessageService.instance.channels[0]
                    self.updateWithChannel()
                }
                else {
                    self.channelLbl.text = "No channels yet!"
                }
            }
        }
    }
    func getMessages(){
        guard let channelId = MessageService.instance.selectedChannel?._id else {return}
        MessageService.instance.findAllMessages(for: channelId) { (success) in
            if success {
                self.tableView.reloadData()
            }
        }
    }
    
    
    
    func setupView(){
        //Shift up the view for keyboard
        view.bindToKeyboard()
        //Set button tap recognizer, set panGesture and tapGesture
        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)),
                          for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        //Add GestureRecognizer for toggle off the keyboard
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toggleOffKeyboard)))
        
        //UI UX
        sendBtn.isHidden = true
        typingStatusLbl.text = ""
        
        //TableView
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    @objc private func toggleOffKeyboard(){
        view.endEditing(true)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    // TableView
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as? MessageCell{
            let msg = MessageService.instance.messages[indexPath.row]
            cell.configureCell(message: msg)
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.instance.messages.count
    }
    
}
