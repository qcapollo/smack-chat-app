//
//  SocketService.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/18/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import UIKit
import SocketIO

class SocketService: NSObject {
    override init() {
        super.init()
    }
    static let instance = SocketService()
    
    var socketM: SocketManager = SocketManager(socketURL: URL(string: BASE_URL)!)
    func establishConnection(){
        socketM.defaultSocket.connect()
    }
    
    func closeConnection(){
        socketM.defaultSocket.disconnect()
    }
    
    func addChannel(channelName: String, channelDescription: String, completion: @escaping CompletionHandler){
        let socket = socketM.defaultSocket
        socket.emit("newChannel", channelName, channelDescription)
        completion(true)
        
    }
    func getChannel(completion: @escaping CompletionHandler){
        let socket = socketM.defaultSocket
        socket.on("channelCreated") { (dataArray, ack) in
            guard let channelName = dataArray[0] as? String
                else {
                    completion(false)
                    return
                    
            }
            guard let channelDescription = dataArray[1] as? String
                else {
                    completion(false)
                    return
                    
            }
            guard let channelId = dataArray[2] as? String
                else {
                    completion(false)
                    return
                    
            }
            
            let newChannel = Channel(_id: channelId, name: channelName, description: channelDescription)
            MessageService.instance.channels.append(newChannel)
            completion(true)
        }
    }
    //Message
    func sendMessage(messageBody: String, userId: String, channelId: String, completion: @escaping CompletionHandler){
        let user = UserDataService.instance
        let socket = socketM.defaultSocket
        socket.emit("newMessage", messageBody, userId, channelId, user.name, user.avatarName, user.avatarColor)
        completion(true)
    }
    func getChatMessage(completion: @escaping (_ newMessage: Message) -> Void){
        let socket = socketM.defaultSocket
        socket.on("messageCreated") { (dataArray, ack) in
            guard let channelId = dataArray[2] as? String else {return}
            guard let msgBody = dataArray[0] as? String else {return}
            //guard let userId = dataArray[1] as? String else {return}
            guard let userName = dataArray[3] as? String else {return}
            guard let userAvatar = dataArray[4] as? String else {return}
            guard let userAvatarColor = dataArray[5] as? String else {return}
            guard let msgId = dataArray[6] as? String else {return}
            guard let timeStamp = dataArray[7] as? String else {return}
            
            let newMessage = Message(message: msgBody, userName: userName, channelId: channelId, userAvatar: userAvatar, userAvatarColor: userAvatarColor, id: msgId, timeStamp: timeStamp)
            
            completion(newMessage)
//            if channelId == MessageService.instance.selectedChannel?._id && AuthService.instance.isLoggedIn {
//
//                MessageService.instance.messages.append(newMessage)
//                completion(true)
//            }else{
//                completion(false)
//            }
        }
    }
    func getTypingUsers(_ completionHandler: @escaping (_ typingUsers: [String : String]) -> Void){
        socketM.defaultSocket.on("userTypingUpdate") { (dataArray, ack) in
            guard let typingUsers = dataArray[0] as? [String: String] else {return}
            completionHandler(typingUsers)
        }
    }
    
    
}
