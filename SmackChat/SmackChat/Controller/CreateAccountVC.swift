//
//  SignUpVC.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/9/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController {
    //Outlets
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var spinWait: UIActivityIndicatorView!
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    //Variables
    var avtName = "profileDefault"
    var avtColor = "[0.5, 0.5, 0.5, 1]"
    var bgColor: UIColor?
    
    //Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
        //
        if UserDataService.instance.avatarName != ""{
            avtName = UserDataService.instance.avatarName
            userImg.image = UIImage(named: avtName)
            //
            if avtName.contains("light") && bgColor == nil  {
                userImg.backgroundColor = UIColor.lightGray
            }
         }
    }

    @IBAction func closeBtnPressed(_ sender: Any) {
        performSegue(withIdentifier: UNWIND_TO_CHANNEL, sender: nil)
    }
    @IBAction func createAccountPressed(_ sender: UIButton) {
        //Set animate
        blurView.isHidden = false
        spinWait.isHidden = false
        spinWait.startAnimating()
        
        //Main code
        guard let email = emailTxt.text, emailTxt.text != "" else {return}
        guard let pwd = passwordTxt.text, passwordTxt.text != "" else {return}
        guard let userName = usernameTxt.text, usernameTxt.text != "" else {return}
        
        AuthService.instance.registerUser(email: email, password: pwd) { (success) in
            if success {
//                print("registered user!")
                AuthService.instance.logInUser(email: email, password: pwd, completion: { (success) in
                    if success {
                        print("Logged In User ",AuthService.instance.authToke)
                        AuthService.instance.createUser(name: userName, email: email, avatarName: self.avtName, avatarColor: self.avtColor, completion: { (success) in
                            if success {
                                
                                print(self.avtColor)
                                
                                self.spinWait.stopAnimating()
                                self.spinWait.isHidden = true
                                self.blurView.isHidden = true
                                self.performSegue(withIdentifier: UNWIND_TO_CHANNEL, sender: nil)
                                
                                NotificationCenter.default.post(name: NOTIF_USERDATA_DID_CHANGE, object: nil)
                            }
                        })
                    }
                })
            }
        }
    }
    @IBAction func pickAvatarPressed(_ sender: Any?) {
        performSegue(withIdentifier: TO_AVATAR_PICKER, sender: nil)
    }
    @IBAction func genBGColorPressed(_ sender: UIButton) {
        let r = CGFloat(arc4random_uniform(255)) / 255
        let g = CGFloat(arc4random_uniform(255)) / 255
        let b = CGFloat(arc4random_uniform(255)) / 255
        bgColor = UIColor(red: r, green: g, blue: b, alpha: 1.0)
        print(bgColor.debugDescription) //For DEBUG
        
        UIView.animate(withDuration: 0.2) {
            self.userImg.backgroundColor = self.bgColor
        }
        //Update the chosen Avt BGcolor for avtColor
        avtColor = "[\(r), \(g), \(b), 1]"
    }
    func setupView(){
        spinWait.isHidden = true
        
        blurView.isHidden = true
        
        usernameTxt.attributedPlaceholder = NSAttributedString(string: "username", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3568627451, green: 0.6235294118, blue: 0.7960784314, alpha: 1)])
        passwordTxt.attributedPlaceholder = NSAttributedString(string: "password", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3568627451, green: 0.6235294118, blue: 0.7960784314, alpha: 1)])
        emailTxt.attributedPlaceholder = NSAttributedString(string: "email", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3568627451, green: 0.6235294118, blue: 0.7960784314, alpha: 1)])
        //
        // Add TapGesture for userImg - works as a button [ACTION]
        userImg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CreateAccountVC.pickAvatarPressed(_:))))
        userImg.isUserInteractionEnabled = true
        
        //Add TapGesture for UIView
        let tap = UITapGestureRecognizer(target: self, action: #selector(CreateAccountVC.viewTappedHandle))
        view.addGestureRecognizer(tap)
    }
    @objc func viewTappedHandle(){
        view.endEditing(true)
    }
}
