//
//  MessageService.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/17/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MessageService {
    static let instance = MessageService()
    
    var channels = [Channel]()
    var selectedChannel: Channel?
    var unreadChannel = [String]()
    var messages = [Message]()
    
    func findAllChannels(completion: @escaping CompletionHandler){
        Alamofire.request(URL_GET_CHANNELS, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            
            if response.result.error == nil {
                guard let Data = response.data else {return}
//
//                do {
//                    self.channels = try JSONDecoder().decode([Channel].self, from: Data)
//                } catch let error  {
//                    debugPrint(error as Any)
//                }
//                print(self.channels)
                do {
                    let json = try? JSON(data: Data).array
                    for item in json!! {
                        let id = item["_id"].stringValue
                        let title = item["name"].stringValue
                        let description = item["description"].stringValue

                        let channel = Channel(_id: id, name: title, description: description)
                        self.channels.append(channel)
                    }
                    NotificationCenter.default.post(name: NOTIF_CHANNELS_LOADED, object: nil)
                    completion(true)
                }
                catch let error as NSError {
                    //error
                    completion(false)
                    debugPrint(error as Any)
                }
            }
            else{
                
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    func findAllMessages(for channelId: String, completion: @escaping CompletionHandler){
        Alamofire.request("\(URL_GET_MESSAGES)\(channelId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error == nil {
                self.clearMessageData()
                guard let data = response.data else {return}
                do{
                    let json = try? JSON(data: data).array
                    for item in json!! {
                        let newMessage = Message(
                            message: item["messageBody"].stringValue,
                            userName: item["userName"].stringValue,
                            channelId: item["channelId"].stringValue,
                            userAvatar: item["userAvatar"].stringValue, userAvatarColor: item["userAvatarColor"].stringValue,
                            id: item["_id"].stringValue,
                            timeStamp: item["timeStamp"].stringValue
                        )
                        self.messages.append(newMessage)
                    }
                    print(self.messages)
                    completion(true)
                }
                catch let error as NSError{
                    
                }
                
            }
            else{
                completion(false)
                debugPrint(response.result.error as Any)
            }
            
        }
        
    }
    func clearChannelData(){
        self.channels.removeAll()
    }
    func clearMessageData(){
        self.messages.removeAll()
    }
}
