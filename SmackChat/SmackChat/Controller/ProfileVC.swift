//
//  ProfileVC.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/15/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    //Outlets
    
    @IBOutlet weak var profileImg: CircleImage!
    @IBOutlet weak var userName_Lbl: UILabel!
    @IBOutlet weak var email_Lbl: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    @IBAction func closeModalPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func logOutPressed(_ sender: Any) {
        AuthService.instance.logOutUser()
        
        NotificationCenter.default.post(name: NOTIF_USERDATA_DID_CHANGE, object: nil)
        
        dismiss(animated: true, completion: nil)
    }
    func setupView(){
        profileImg.image = UIImage(named: UserDataService.instance.avatarName)
        profileImg.backgroundColor = UserDataService.instance.convertToUIColor(avtColorStr: UserDataService.instance.avatarColor)
        userName_Lbl.text = UserDataService.instance.name
        email_Lbl.text = UserDataService.instance.email
        //Add tapGesture for bgView to dismiss this VC
        bgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProfileVC.closeModalPressed(_:))))
        bgView.isUserInteractionEnabled = true
    }
}
