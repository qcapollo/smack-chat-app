//
//  MessageCell.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/19/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    @IBOutlet weak var userAvatar: CircleImage!
    @IBOutlet weak var userName_Lbl: UILabel!
    @IBOutlet weak var timeStamp_Lbl: UILabel!
    @IBOutlet weak var msgBody_Lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(message: Message){
        msgBody_Lbl.text = message.message
        userName_Lbl.text = message.userName
        userAvatar.image = UIImage(named: message.userAvatar)
        userAvatar.backgroundColor = UserDataService.instance.convertToUIColor(avtColorStr: message.userAvatarColor)
        
        guard var isoDate = message.timeStamp else {return}
        let end = isoDate.index(isoDate.endIndex, offsetBy: -5)
        isoDate = isoDate.substring(to: end)
        
        let isoFormatter = ISO8601DateFormatter()
        let chatDate = isoFormatter.date(from: isoDate.appending("Z"))
        
        let newFormatter = DateFormatter()
        newFormatter.dateFormat = "MMM d, h:mm a"
        
        if let finalDate = chatDate {
            let date = newFormatter.string(from: finalDate)
            timeStamp_Lbl.text = date
        }
    }

}
