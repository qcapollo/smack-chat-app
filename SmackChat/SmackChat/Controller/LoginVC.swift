//
//  LoginVC.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/9/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var spinWait: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    @IBAction func closePressed(_ sender: Any) {
        performSegue(withIdentifier: UNWIND_TO_CHANNEL, sender: nil)
    }
    @IBAction func loginBtnPressed(_ sender: UIButton) {
        blurView.isHidden = false
        spinWait.isHidden = false
        spinWait.startAnimating()
        //
        guard let username = emailTxt.text, emailTxt.text != ""
            else {return}
        guard let pass = passwordTxt.text, passwordTxt.text != ""
            else {return}
        AuthService.instance.logInUser(email: username, password: pass) { (success) in
            if success {
                print("Logged In!", AuthService.instance.authToke)
                AuthService.instance.findUserByEmail(completion: { (success) in
                    if success {
                        //Some visual effects
                        self.blurView.isHidden = true
                        self.spinWait.isHidden = true
                        self.spinWait.stopAnimating()
                        //
                        NotificationCenter.default.post(name: NOTIF_USERDATA_DID_CHANGE, object: nil)
                        self.performSegue(withIdentifier: UNWIND_TO_CHANNEL, sender: nil)
                    }else{
                        print("Logging fail")
                    }
                })
            }
//          else{}
        }
        
    }
    @IBAction func signUpBtnPressed(_ sender: Any) {
        performSegue(withIdentifier: TO_CREATE_ACCOUNT, sender: nil)
    }
    func setupView(){
        UIApplication.shared.statusBarStyle = .default
        blurView.isHidden = true
        spinWait.isHidden = true
        //
        emailTxt.attributedPlaceholder = NSAttributedString(string: "email", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3568627451, green: 0.6235294118, blue: 0.7960784314, alpha: 1)])
        passwordTxt.attributedPlaceholder = NSAttributedString(string: "password", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3568627451, green: 0.6235294118, blue: 0.7960784314, alpha: 1)])
        //add TapGesture for toggle off the virtual keyboard
        view.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(self.mainViewTapHandler))
        )
    }
    @objc func mainViewTapHandler(){
        view.endEditing(true)
    }
}
