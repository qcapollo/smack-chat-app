//
//  Channel.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/17/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import Foundation

//struct Channel: Decodable {
//    public private(set) var _id: String!
//    public private(set) var name: String!
//    public private(set) var description: String!
//    public private(set) var __v: Int?
//}

struct Channel {
    public private(set) var _id: String!
    public private(set) var name: String!
    public private(set) var description: String!
}
