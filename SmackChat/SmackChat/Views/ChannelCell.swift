//
//  ChannelCell.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/17/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import UIKit

class ChannelCell: UITableViewCell {

    @IBOutlet weak var channelName_Lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.layer.backgroundColor = UIColor(white: 1, alpha: 0.2).cgColor
        }
        else{
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    func configureCell(channel: Channel){
        let title = channel.name ?? ""
        self.channelName_Lbl.text = "#\(title)"
        self.channelName_Lbl.font = UIFont(name: "AvenirNext-Regular", size: 18)
        
        for id in MessageService.instance.unreadChannel {
            if id == channel._id {
                channelName_Lbl.font = UIFont(name: "AvenirNext-Heavy", size: 18)
            }
        }
    }
    func setupView(){
        
    }
}
