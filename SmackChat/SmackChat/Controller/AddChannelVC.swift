//
//  AddChannelVC.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/17/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import UIKit

class AddChannelVC: UIViewController {
    //Outlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var channelNameTxt: UITextField!
    @IBOutlet weak var channelDesTxt: UITextField!
    @IBOutlet weak var mainView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()
    }
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    //Actions
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func createChannelPressed(_ sender: Any) {
        guard let channelName = channelNameTxt.text, channelNameTxt.text != "" else {return}
        guard let channelDescription = channelDesTxt.text, channelDesTxt.text != "" else {return}
        SocketService.instance.addChannel(channelName: channelName, channelDescription: channelDescription) { (success) in
            
            if success {
                //Add channel data to client
                self.dismiss(animated: true, completion: nil)
            }
            else{
                
            }
        }
    }
    //Function
    func setupView(){
        //Set placeholder's textcolor
        channelNameTxt.attributedPlaceholder = NSAttributedString(string: "name", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3568627451, green: 0.6235294118, blue: 0.7960784314, alpha: 1)])
        
        channelDesTxt.attributedPlaceholder = NSAttributedString(string: "description", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3568627451, green: 0.6235294118, blue: 0.7960784314, alpha: 1)])
        //Add gesture for dismiss this VC
        bgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(AddChannelVC.closeBtnPressed(_:))))
        
//        //Add gesture for toggle off virtual keyboard
        mainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.mainViewTapHandler)))
        
    }
    @objc func mainViewTapHandler(){
        view.endEditing(true)
    }
}
