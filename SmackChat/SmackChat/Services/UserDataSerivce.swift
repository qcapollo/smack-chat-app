//
//  UserDataSerivce.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/12/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import Foundation
class UserDataService {
    static let instance = UserDataService()
    
    public private(set) var id = ""
    public private(set) var avatarColor = ""
    public private(set) var avatarName = ""
    public private(set) var email = ""
    public private(set) var name = ""
    
    func setUserData (id: String, avatarColor: String, avatarName: String, email: String, name: String){
        self.id = id
        self.avatarColor = avatarColor
        self.avatarName = avatarName
        self.email = email
        self.name = name
    }
    func setAvatarName(avatarName: String){
        self.avatarName = avatarName
    }
    func setAvatarColor(avatarColor: String){
        self.avatarColor = avatarColor
    }
    
    func convertToUIColor(avtColorStr: String) -> UIColor {
        let scanner = Scanner(string: avtColorStr)
        let skip = CharacterSet(charactersIn: "[], ")
        let comma = CharacterSet(charactersIn: ",")
        scanner.charactersToBeSkipped = skip
        
        var r, g, b, a: NSString?

        scanner.scanUpToCharacters(from: comma, into: &r)
        scanner.scanUpToCharacters(from: comma, into: &g)
        scanner.scanUpToCharacters(from: comma, into: &b)
        scanner.scanUpToCharacters(from: comma, into: &a)
        
        let defaultColor = UIColor.lightGray
        //if the upwrapping got failure(s)
        guard let rUnwrapped = r else {return defaultColor}
        guard let gUnwrapped = g else {return defaultColor}
        guard let bUnwrapped = b else {return defaultColor}
        guard let aUnwrapped = a else {return defaultColor}
        
        return UIColor(
            red: CGFloat(rUnwrapped.doubleValue),
            green: CGFloat(gUnwrapped.doubleValue),
            blue: CGFloat(bUnwrapped.doubleValue),
            alpha: CGFloat(aUnwrapped.doubleValue)
        )
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
