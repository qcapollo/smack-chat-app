//
//  ChannelVC.swift
//  SmackChat
//
//  Created by LuckCornKoo on 5/9/18.
//  Copyright © 2018 NguyenQuocCuong. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var userAvatarImg: UIImageView!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func prepareForUnwindWithSegue(_ segue: UIStoryboardSegue){
        //updateView with loggedIn User's info
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        tableView.delegate = self
        tableView.dataSource = self
        //Persistent connections
        SocketService.instance.getChannel { (success) in
            if success {
                self.tableView.reloadData()
            }
        }
        SocketService.instance.getChatMessage { (newMessage) in
            if newMessage.channelId != MessageService.instance.selectedChannel?._id && AuthService.instance.isLoggedIn {
                MessageService.instance.unreadChannel.append(newMessage.channelId)
                self.tableView.reloadData()
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        setupUserInfo()
    }
    
    //Actions
    @IBAction func addChannelPressed(_ sender: Any) {
        if !AuthService.instance.isLoggedIn {
            performSegue(withIdentifier: TO_LOGIN, sender: self)
        }
        else {
            //show "addChannel" View
            let addChannel = AddChannelVC()
            addChannel.modalPresentationStyle = .custom
            present(addChannel, animated: true, completion: nil)
        }
    }
    @IBAction func loginBtnPressed(_ sender: Any) {
        if !AuthService.instance.isLoggedIn {
            performSegue(withIdentifier: TO_LOGIN, sender: self)
        }
        else {
            //show Profile Info
            let profile = ProfileVC()
            profile.modalPresentationStyle = .custom
            present(profile, animated: true, completion: nil)
            
        }
    }
    
    @objc func userDataDidChange(_ notif: Notification){
        setupUserInfo()
    }
    @objc func channelsLoaded(_ notif: Notification) {
        tableView.reloadData()
    }
    func setupView(){
        // Do any additional setup after loading the view.
        self.revealViewController().rearViewRevealWidth = CGFloat(self.view.frame.size.width - 100)
        
        //Add notification observer
        NotificationCenter.default.addObserver(self, selector: #selector(ChannelVC.userDataDidChange(_:)), name: NOTIF_USERDATA_DID_CHANGE, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.channelsLoaded(_:)), name: NOTIF_CHANNELS_LOADED, object: nil)
        
        //add GestureRecognizer for User's Avatar shown on-the-left Menu
    userAvatarImg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ChannelVC.loginBtnPressed(_:))))
        userAvatarImg.isUserInteractionEnabled = true
        
    }
    func setupUserInfo(){
        if AuthService.instance.isLoggedIn {
            //
            loginBtn.setTitle(UserDataService.instance.name, for: .normal)
            
            userAvatarImg.image = UIImage(named: UserDataService.instance.avatarName)
            
            userAvatarImg.backgroundColor = UserDataService.instance.convertToUIColor(avtColorStr: UserDataService.instance.avatarColor)
        }
        else {
            //
            loginBtn.setTitle("Login", for: .normal)
            userAvatarImg.image = UIImage(named: "menuProfileIcon")
            userAvatarImg.backgroundColor = UIColor.clear
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "channelCell", for: indexPath) as? ChannelCell {
            let channel = MessageService.instance.channels[indexPath.row]
            cell.configureCell(channel: channel)
            return cell
        }
        else{
            return UITableViewCell()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.instance.channels.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = MessageService.instance.channels[indexPath.row]
        MessageService.instance.selectedChannel = channel
        //
        if MessageService.instance.unreadChannel.count > 0 {
            MessageService.instance.unreadChannel = MessageService.instance.unreadChannel.filter{$0 != channel._id}
        }
        let index = IndexPath(row: indexPath.row, section: 0)
        tableView.reloadRows(at: [index], with: .none)
        tableView.selectRow(at: index, animated: false, scrollPosition: .none)
        //
        NotificationCenter.default.post(name: NOTIF_CHANNEL_SELECTED, object: nil)
        //Make menu slide back to ChatVC
        self.revealViewController().revealToggle(animated: true)
    }
    
}
